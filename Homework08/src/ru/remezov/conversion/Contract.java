package ru.remezov.conversion;

public class Contract {
    protected int number;
    protected String date;
    protected String[] goods;

    public Contract(int number, String date, String... goods) {
        this.number = number;
        this.date = date;
        this.goods = goods;
    }

    public int getNumber() {
        return number;
    }

    public String getDate() {
        return date;
    }

    public String[] getGoods() {
        return goods;
    }
}
