package ru.remezov.conversion;

public class Act {
    protected int number;
    protected String date;
    protected String[] goods;

    public Act(int number, String date, String... goods) {
        this.number = number;
        this.date = date;
        this.goods = goods;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setGoods(String[] goods) {
        this.goods = goods;
    }
}
