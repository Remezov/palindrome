package VM;

public class VM {

    public VM() {
    }

    public int table1()
    {
        System.out.println("\n1 - Menu");
        System.out.println("2 - Put your money");
        System.out.println("3 - Choose a drink");
        return 0;
    }
    public int table2()
    {
        for (int i = 0; i <= 3; i++)
        {
            System.out.println(i+1+ " " +Drink.values()[i]+ " " +Drink.values()[i].getCost()+" RUB");
        }
        return 0;
    }
    public int deliveryOfGoods(int number, int money){
        if (money >= Drink.values()[number - 1].getCost())
            System.out.println("Get your "+Drink.values()[number - 1]+" and change");
        else System.out.println("Insufficient funds. Get your money.");
        return 0;
    }
    public int enteringMoney(int money)
    {
        System.out.println("\n Your limit is "+money+" RUB");
        return 0;
    }
}
