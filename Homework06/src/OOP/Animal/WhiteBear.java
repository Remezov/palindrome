package OOP.Animal;

public class WhiteBear extends Bear {
    protected String place;
    public WhiteBear(String name, String place) {
        super(name);
        this.place = place;
    }
    @Override
    public String makeSound()
    {
        return "I live in " +this.place+ "!";
    }

    public String dream() {
        return "I want to go Arctic!";
    }
}
