package ru.remezov.animal;

public abstract class Man extends Animal implements Running, Swimming{

    protected String employee;

    public Man(String name, String employee) {
        super(name);
        this.employee = employee;
    }

    @Override
    public void getName() {
        System.out.println("I'm " +this.name+ "!");
    }
}
