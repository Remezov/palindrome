package ru.remezov.animal;

public interface Swimming {

    default void canSwim() {
        System.out.println("I can swim!");
    }
}
